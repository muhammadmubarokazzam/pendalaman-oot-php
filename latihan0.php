<?php
//1. Constructor dan  Destructor
    class hewan {
        //harus membuat properti yg dimana bisa dipakai di setiap method yg ada didalam kelas hewan ini
        public $nama = "singa";
        public $jenis = "karnivora";
         
        // method(function)                         dinamik value
        public function __construct($nama,$jenis = "karnifora"){

            //ini set, supaya bisa mengubah properti nama dan jenis
            $this->nama = $nama;
            $this->jenis = $jenis;
            echo "ini adalah ". $this->nama .", dia adalah jenis hewan ". $this->jenis . "<br>";
            
        }

        public function __destruct(){
            echo "data hewan ".$this->nama." sudah hilang"."<br>";

        }






    }
    //instansiasi, memanggil method construct (dinamic)
    $meong = new hewan("kucing");
    $moo = new hewan("sapi","herbifora");
    $citcit = new hewan("tikus","omnifora");

    //destruc bukan untuk menghapus objek, tapi dipanggil ketika objeknya dihapus dalam memori
    echo "<br>";
    unset($citcit);









?>