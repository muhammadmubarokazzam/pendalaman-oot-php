<?php
//2. Inheritance (pewarisan)
    class tumbuhan {
        protected $biji;
        protected $nama;
        
        public function setbiji($biji){
            $this->biji = $biji;
        }

        public function setnama($nama){
            $this->nama = $nama;
        }

        public function getbiji(){
            echo "ini adalah tumbuhan berbiji " . $this->biji;
        }

        public function getnama(){
            echo "yaitu tumbuhan bernama " . $this->nama;
        }
}

    class plant0 extends tumbuhan{
        public $biji = "monokotil";
        public $nama = "mangga";

        public function getbiji(){
            echo "tanaman berbiji " . $this->biji;
        }

        public function getnama(){
            echo "salah satunya adalah " . $this->nama;
        }
    }

    class tumbuhan1 extends tumbuhan{
    }

    class tumbuhan0 extends plant0{
    }

    //pewarisan 2

    $mangga0 = new tumbuhan0 ();
    $mangga0->getbiji();
    echo "<br>";
    $mangga1 = new tumbuhan0 ();
    $mangga1->getnama();
    echo "<br>";

    //pewarisan 1

    echo "<br>";
    $kacang0 = new tumbuhan1 ();
    $kacang0->setbiji("dikotil");
    $kacang0->getbiji();
    echo "<br>";
    $kacang1 = new tumbuhan1 ();
    $kacang1->setnama("kacang tanah");
    $kacang1->getnama();
?>