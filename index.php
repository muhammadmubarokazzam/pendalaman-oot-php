<?php
abstract class Hewan {
    public $hewan0;

    public function __construct($hewan0) {
        $this->nama = $hewan0;
        $this->jumlahkaki = $hewan0;
        $this->keahlian = $hewan0;
    }
    abstract public function intro();
  }

  abstract class Fight {
    public $darah;
    public $tempur0;
    public $diserang;

    public function __construct($tempur0) {
        $this->atkpwr = $tempur0;
        $this->defpwr = $tempur0;
        $this->diserang = $tempur0;
    }

    abstract public function intro();
  }


  
class animal extends Hewan {
    public function intro() : string {
        return "Nama Hewan : $this->nama";
    }
}
class animal0 extends Hewan {
    public function intro() : string {
        return ", Jumlah kaki sebanyak $this->jumlahkaki";
    }
}
class animal1 extends Hewan {
    public function intro() : string {
        return ", Keahlian yaitu $this->keahlian";
    }
}
class animal2 extends Fight {
    public function intro() : string {
        return ", Attack Power sebesar $this->atkpwr";
    }
}
class animal3 extends Fight {
    public function intro() : string {
        return ", Defence Power sebesar $this->defpwr";
    }
}
class serang extends Hewan {
    public function intro() : string {
        return "$this->nama, menyerang";
    }
}

class diserang extends Fight {
    public function intro() : string {
        return "Hewan yang diserang berkurang - $this->diserang";
    }
}

$set = new animal(" Harimau ");
echo $set->intro();
$set0 = new animal0(" 4");
echo $set0->intro();
$set1 = new animal1(" Lari Cepat");
echo $set1->intro();
$set2 = new animal2(" 7");
echo $set2->intro();
$set3 = new animal3(" 8");
echo $set3->intro();
echo "<br>";

$set = new animal(" Elang ");
echo $set->intro();
$set0 = new animal0(" 2");
echo $set0->intro();
$set1 = new animal1(" Terbang tinggi");
echo $set1->intro();
$set2 = new animal2(" 10");
echo $set2->intro();
$set3 = new animal3(" 5");
echo $set3->intro();
echo "<br>";
echo "<br>";

$set = new serang(" Elang ");
echo $set->intro();
echo "<br>";
$set0 = new diserang(" 8");
echo $set0->intro();
echo "<br>";

$set = new serang(" Harimau ");
echo $set->intro();
echo "<br>";
$set0 = new diserang(" 5");
echo $set0->intro();
echo "<br>";

?>