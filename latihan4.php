<?php
//5. Abstract Class
// Parent class
//tambah abstrack
abstract class Car {
  public $name;
  public function __construct($name) {
    $this->name = $name;
  }
  //memiliki 1 method abstrack
  abstract public function intro();
}

// Child classes
class Audi extends Car {
    //sama seperti method abstrack
  public function intro() : string {
    return "Choose German quality! I'm an $this->name!";
  }
}

class Volvo extends Car {
    //sama seperti method abstrack
  public function intro() : string {
    return "Proud to be Swedish! I'm a $this->name!";
  }
}

class Citroen extends Car {
    //sama seperti method abstrack
  public function intro() : string {
    return "French extravagance! I'm a $this->name!";
  }
}

// Create objects from the child classes
//abstrack tdk bisa instan inisiasi
$audi = new audi("Audi");
echo $audi->intro();
echo "<br>";

$volvo = new volvo("Volvo");
echo $volvo->intro();
echo "<br>";

$citroen = new citroen("Citroen");
echo $citroen->intro();

//kegunaan dari abstrak : class memiliki class turunan, method harus ada dikelas masing-masing, berbeda-beda maka harus ada
// karena haarus ada method intro dan method intro disetiap anak berbeda, maka perlu abstrak
// harus punya 1 abstrak method, dan anaknya mengimplementasikan yg parentnya


?>