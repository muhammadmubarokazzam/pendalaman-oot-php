<?php
//4. Static Keyword dan Constant
class Singa
{

  public static $KAKI = 4;

  public static function lari()
  {
     echo 'Singa berlari';

   }

}
//:: artinya menggunakan statik method/statik properti,
//tidak perlu lagi menginstansiasi sebuah klass utk mengambil properti atau methodnya
echo Singa::$KAKI; // 4

echo PHP_EOL;

echo Singa::lari(); // Singa berlari

echo PHP_EOL;