<?php
//6. Interface
//isi hanya method abstrak, keyword abstrak tidak perlu ditulis
interface HewanInterface {

  public function getJenis();

}

class Kambing implements HewanInterface {

  public function getJenis() {

     return 'Herbivora';

  }

}

class Harimau implements HewanInterface {

   public function getJenis() {

      return 'Karnivora';

   }

}

class Singa implements HewanInterface {

   public function getJenis() {

     return 'Karnivora';

   }
//Interface tidak dapat memiliki properti, sedangkan abstract class dapat
//Semua metode Interface harus bersifat publik, sedangkan metode abstract class bersifat publik atau dilindungi(protected)
//Semua metode dalam Interface adalah abstract, sehingga tidak dapat diimplementasikan dalam kode dan kata kunci abstract tidak diperlukan
//Kelas dapat mengimplementasikan Interface sambil mewarisi dari kelas lain pada saat yang sama
//Tidak seperti pada pewarisan, pada interface kita dapat menggunakan multiple interface sekaligus 
}