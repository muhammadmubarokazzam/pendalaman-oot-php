<?php
 
class Singa
{
   public static $KAKI = 4;

   public function kaki1()
   {
      echo Singa::$KAKI;
   }
 
   public function kaki2()
   {
      echo self::$KAKI;
   }
 
    public function kaki3()
    {
      echo static::$KAKI;
    }

    public static function getKaki()
    {
       return self::$KAKI;
    }

    public static function tampilkanKaki()
    {
        echo self::getKaki();
     }
 
}
 
$singa = new Singa();
echo $singa->kaki1(); //4
echo PHP_EOL;
echo $singa->kaki2(); //4
echo PHP_EOL;
echo $singa->kaki3(); //4
echo PHP_EOL;
//langsung memanggil class singa, kemudian tampilkan kakinya
echo Singa::tampilkanKaki(); //4