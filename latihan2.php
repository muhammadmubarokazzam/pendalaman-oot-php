<?php
//3. Visibility 
class Avenger {
  public $name = "mind stone"; 
  protected $color = "luarnya kuning dalamnya putih";
  private $weight = "berasal dari tongkat loki";

  //(private hanya bisa diakses dari kelas tsb)
  public function getweight(){
        echo $this->weight;
    }

}

//(protected)
class mindstone extends Avenger{
    public function getcolor(){
        echo $this->color;
    }
}
//akses diluar kelas (publik)
$infinitestone = new Avenger();
echo $infinitestone->name; // OK

echo "<br>";

//akses dengan tambahan class (protected)
$mindstone0 = new mindstone;
$mindstone0->getcolor();

echo "<br>";

//akses dengan tambahan didalam class (private)
$mindstone1 = new mindstone;
$mindstone1->getweight();
//atau
//$batu = new Avenger;
//echo $batu->getweight();

?>